// https://g.co/bard/share/06a7ab6735ab

import java.util.Scanner;

public class PieSlices {

    public static void main(String[] args) {
        int numSlices, numPeople, slicesPerPerson, remainingSlices;

        // Get input from command line arguments or prompt the user
        if (args.length == 2) {
            numSlices = Integer.parseInt(args[0]);
            numPeople = Integer.parseInt(args[1]);
        } else {
            Scanner scanner = new Scanner(System.in);
            System.out.print("Enter the number of slices of pie: ");
            numSlices = scanner.nextInt();
            System.out.print("Enter the number of people: ");
            numPeople = scanner.nextInt();
            scanner.close();
        }

        // Share the pie and get the results
        int[] results = sharePie(numSlices, numPeople);
        slicesPerPerson = results[0];
        remainingSlices = results[1];

        // Print the results
        System.out.println("Each person gets " + slicesPerPerson + " slices.");
        System.out.println("There are " + remainingSlices + " slices left over.");
    }

    public static int[] sharePie(int numSlices, int numPeople) {
	if (numPeople == 0) {
	    throw new IllegalArgumentException("Cannot share pie with 0 people.");
	}
	if (numSlices < 0) {
	    throw new IllegalArgumentException("Number of slices cannot be negative.");
	}
        int slicesPerPerson = numSlices / numPeople;
        int remainingSlices = numSlices % numPeople;
        return new int[] {slicesPerPerson, remainingSlices};
    }
}
