/*
 * Command to execute tests follows (pathnames may vary depending on OS):
 * java -cp .:/usr/share/java/junit.jar:/usr/share/java/hamcrest/hamcrest.jar org.junit.runner.JUnitCore PieSlicesTest
 */

import org.junit.Test;
import static org.junit.Assert.*;

public class PieSlicesTest {

    @Test
    public void testSharePieEven() {
        int[] results = PieSlices.sharePie(10, 2);
        assertEquals(5, results[0]); // slicesPerPerson
        assertEquals(0, results[1]); // remainingSlices
    }

    @Test
    public void testSharePieUneven() {
        int[] results = PieSlices.sharePie(12, 3);
        assertEquals(4, results[0]);
        assertEquals(0, results[1]);
    }

    @Test
    public void testSharePieWithRemainder() {
        int[] results = PieSlices.sharePie(7, 2);
        assertEquals(3, results[0]);
        assertEquals(1, results[1]);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSharePieNegativeSlices() {
        PieSlices.sharePie(-5, 2); // Negative number of slices
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSharePieZeroPeople() {
        PieSlices.sharePie(10, 0); // Zero people
    }

    @Test(expected = NumberFormatException.class)
    public void testMainInvalidCommandLineArgs() {
        String[] args = {"abc", "4"}; // Non-numeric argument
        PieSlices.main(args);
    }
}
