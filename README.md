# study

You may need to install [Junit](https://junit.org/junit5/) for testing ... 

## PieSlices

Run the Java code (example of 18 slices for 5 people):

`java PieSlices.java 18 5`

Compile classes (pathnames may vary depending on OS):

`javac PieSlices.java`

`javac -cp .:/usr/share/java/junit.jar PieSlicesTest.java`

Command to execute tests follows (pathnames may vary depending on OS):

`java -cp .:/usr/share/java/junit.jar:/usr/share/java/hamcrest/hamcrest.jar org.junit.runner.JUnitCore PieSlicesTest`

Bard session:
https://g.co/bard/share/ea4448e6ab84
(was)
https://g.co/bard/share/33f6cce21c07
